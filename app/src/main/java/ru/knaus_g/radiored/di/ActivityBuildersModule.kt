package ru.knaus_g.radiored.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.knaus_g.radiored.di.main.MainModule
import ru.knaus_g.radiored.di.main.MainScope
import ru.knaus_g.radiored.di.main.MainViewModelModule
import ru.knaus_g.radiored.di.service.ForegroundServiceModule
import ru.knaus_g.radiored.service.ForegroundService
import ru.knaus_g.radiored.ui.main.MainActivity


@Module
abstract class ActivityBuildersModule {

    @MainScope
    @ContributesAndroidInjector(
        modules =
        [
            MainModule::class,
            MainViewModelModule::class
        ]
    )
    abstract fun contributeMainActivity(): MainActivity


    @MainScope
    @ContributesAndroidInjector(
        modules = [
        ForegroundServiceModule::class
        ]
    )
    abstract fun contributeForegroundService(): ForegroundService
}