package ru.knaus_g.radiored.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import dagger.Module
import dagger.Provides
import ru.knaus_g.radiored.room.AppDatabase
import ru.knaus_g.radiored.ui.main.MainActivity
import ru.knaus_g.radiored.ui.main.RadioRecyclerAdapter
import javax.inject.Singleton


@Module
class AppModule {

    @Singleton
    @Provides
    fun providesDataBase(application: Application) =
        Room.databaseBuilder(application.applicationContext, AppDatabase::class.java, "myDB")
            .addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {

                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Маруся FM', 'http://radio-holding.ru:9000/marusya_default', 'http://top-radio.ru/assets/image/radio/180/radiomarusya.jpg', 'POP', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Roks', 'http://online-radioroks.tavrmedia.ua/RadioROKS', 'https://play.tavr.media/static/image/roks/RadioROKS_logo.png', 'ROCK', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Rock FM', 'https://nashe1.hostingradio.ru:80/rock-128.mp3', 'https://cdn.webrad.io/images/logos/radio-pp-ru/radio-rock-fm-95-2.png', 'ROCK', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Chocolate', 'https://choco.hostingradio.ru:10010/fm', 'https://cdn.webrad.io/images/logos/radio-pp-ru/radio-sokolad-98-fm.png', 'WORLD HITS', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('DFM', 'https://dfm.hostingradio.ru/dfm96.aacp', 'https://cdn.webrad.io/images/logos/radio-pp-ru/radio-difm-101-2.png', 'POP', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Energy', 'https://pub0201.101.ru/stream/air/aac/64/99', 'https://cdn.webrad.io/images/logos/radio-pp-ru/radio-energy-104-2-fm.png', 'POP', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Relax', 'https://pub0301.101.ru:8443/stream/air/aac/64/200', 'https://cdn.webrad.io/images/logos/radio-pp-ru/radio-relax-90-8-fm.png', 'RELAX', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Медляк', 'http://air2.radiorecord.ru:9003/mdl_320', 'http://top-radio.ru/assets/image/radio/180/recordmedlyak.png', 'SLOWLY POP', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('DEEP', 'https://r163-172-140-159.relay.radiotoolkit.com:30003/rcmdeep', 'https://radiopotok.ru/f/station/64/1496.png', 'DEEP HOUSE', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Радио Мегахит', 'http://listen.megahit.online/megamp128?time=1590681292240', 'http://www.radiobells.com/stations/megahitonline_100.jpg', 'POP', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Lounge FM', 'https://cast.radiogroup.com.ua/loungefm', 'http://www.radiobells.com/stations/loungechillout_100.jpg', 'RELAX', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Rock Radio', 'http://air2.radiorecord.ru:9003/rock_320', 'http://www.radiobells.com/stations/recordrock_100.jpg', 'ROCK', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Comedy Radio', 'https://pub0101.101.ru/stream/air/aac/64/202', 'http://www.radiobells.com/stations/comediradio.jpg', 'TALK', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('FEEL FM', 'https://r51-158-108-249.relay.radiotoolkit.com:30003/feelfm', 'http://www.radiobells.com/stations/feelfm_100.jpg', 'DEEP HOUSE', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Radio GAME PLAY', 'http://62.109.3.230/gameplay.mp3', 'http://www.radiobells.com/stations/gameplay.jpg', 'GAMES MUSIC', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Казак FM', 'http://193.242.149.55:8000/kfm?0.5488886432005484', 'http://www.radiobells.com/stations/kazak.jpg', 'FOLK', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Dodo Fm', 'http://dodofm.ru:8000/radio', 'http://www.radiobells.com/stations/dodofm_100.jpg', 'POP', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Супердискотека 90-х', 'http://air2.radiorecord.ru:805/sd90_320', 'http://www.radiobells.com/stations/super90_100.jpg', 'RETRO', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('HotMix Hip-hop', 'http://streaming.hotmixradio.fr/hotmixradio-hiphop-64.aac', 'http://www.radiobells.com/stations/hotmixhiphop_100.jpg', 'RAP', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('HIPHOPBY', 'http://online.hiphop.by:9000/8000', 'http://www.radiobells.com/stations/hiphopby.jpg', 'RAP', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Симфония FM', 'http://air.radiorecord.ru:805/symph_320', 'http://www.radiobells.com/stations/symphonyfm.jpg', 'CLASSIC', 0, 0, 0)")
                    db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Радио Орфей', 'http://orfeyfm.hostingradio.ru:8034/orfeyfm128.mp3', 'http://www.radiobells.com/stations/orfey.jpg', 'CLASSIC', 0, 0, 0)")
                    //db.execSQL("INSERT INTO radio (name, radioStream, image, genre, isPlaying, isPlayerTarget, isFavorite) VALUES ('Название', 'поток', 'картинка', 'жанр', 0, 0, 0)")


                    db.execSQL("INSERT INTO settings (name, value) VALUES ('MainFavorite', '0')")
                    //db.execSQL("INSERT INTO settings (name, value) VALUES ('', '')")
                }
            })
            .build()


    @Singleton
    @Provides
    fun providesRadioDao(database: AppDatabase) = database.radioDao()


    @Singleton
    @Provides
    fun provideSettingsDao(database: AppDatabase) = database.settingsDao()



    @Singleton
    @Provides
    fun getContext(application: Application) = application.applicationContext
}