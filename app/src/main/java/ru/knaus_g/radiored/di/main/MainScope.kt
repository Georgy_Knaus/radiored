package ru.knaus_g.radiored.di.main

import javax.inject.Scope

@MustBeDocumented
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope
