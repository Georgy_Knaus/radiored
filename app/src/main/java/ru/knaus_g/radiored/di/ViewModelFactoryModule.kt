package ru.knaus_g.radiored.di

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import ru.knaus_g.radiored.viewmodels.ViewModelFactory

@Module
abstract class ViewModelFactoryModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}