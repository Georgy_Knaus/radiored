package ru.knaus_g.radiored.di.main

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import ru.knaus_g.radiored.di.ViewModelKey
import ru.knaus_g.radiored.ui.main.MainViewModel

@Module
abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    internal abstract fun bindViewModel(mainModel: MainViewModel): ViewModel

}