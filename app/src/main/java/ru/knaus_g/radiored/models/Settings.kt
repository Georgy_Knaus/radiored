package ru.knaus_g.radiored.models

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "settings")
data class Settings(
    @PrimaryKey var name: String,
    var value: Int?

){
    constructor() : this("", null

    )
}