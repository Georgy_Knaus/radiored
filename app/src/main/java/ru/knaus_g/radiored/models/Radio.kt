package ru.knaus_g.radiored.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "radio")
data class Radio(
    @PrimaryKey var name: String,
    var radioStream: String,
    var image: String,
    var genre: String,
    var isPlaying: Int = 0,
    var isPlayerTarget: Int = 0,
    var isFavorite: Int = 0
) {

    constructor() : this("", "", "", "", 0, 0, 0

    )


    override fun equals(other: Any?): Boolean {

        if (javaClass != other?.javaClass) {
            return false
        }

        other as Radio

        if (name != other.name) {
            return false
        }

        if (radioStream != other.radioStream) {
            return false
        }

        if (genre != other.genre) {
            return false
        }

        if (image != other.image) {
            return false
        }

        if (isPlaying != other.isPlaying) {
            return false
        }

        if (isPlayerTarget != other.isPlayerTarget) {
            return false
        }

        if (isFavorite != other.isFavorite) {
            return false
        }
        return true

    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + radioStream.hashCode()
        result = 31 * result + image.hashCode()
        result = 31 * result + genre.hashCode()
        result = 31 * result + isPlaying.hashCode()
        result = 31 * result + isFavorite.hashCode()
        result = 31 * result + isPlayerTarget.hashCode()
        return result
    }
}