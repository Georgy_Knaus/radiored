package ru.knaus_g.radiored.room

import androidx.lifecycle.LiveData
import androidx.room.*
import ru.knaus_g.radiored.models.Settings


@Dao
interface SettingsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addSettings(setting: Settings)

    @Update
    suspend fun updateSettings(setting: Settings)

    @Delete
    suspend fun deleteSettings(setting: Settings)


    @Query("SELECT * FROM settings WHERE name =:name")
    fun getSettings(name: String): LiveData<Settings>


}