package ru.knaus_g.radiored.room

import androidx.lifecycle.LiveData
import androidx.room.*
import ru.knaus_g.radiored.models.Radio

@Dao
interface RadioDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addRadio(radio: Radio)

    @Update
    suspend fun updateRadio(radio: Radio)

    @Delete
    suspend fun deleteRadio(radio: Radio)

    @Query("SELECT * FROM radio ORDER BY name")
    fun getAllRadioSortName(): LiveData<List<Radio>>

    @Query("SELECT * FROM radio WHERE isFavorite = 1 ORDER BY name")
    fun getAllRadioFavorite(): LiveData<List<Radio>>

    @Query("UPDATE radio SET isPlaying = 'false' WHERE name != :name")
    suspend fun stopAll(name: String)

    @Query("UPDATE radio SET isPlaying = 'false'")
    suspend fun startApp()

    @Query("UPDATE radio SET isPlayerTarget = 'false'")
    suspend fun deleteTarget()

    @Query("SELECT isPlaying FROM radio WHERE name =:name")
    suspend fun getIsPlaying(name: String): Boolean

    @Query("UPDATE radio SET isPlaying = 'false' AND isPlayerTarget = 'false'")
    suspend fun clearAll()

    @Query("SELECT * FROM radio WHERE name like :name ORDER BY isFavorite DESC")
    fun searchFromData(name: String): LiveData<List<Radio>>


    @Query("UPDATE radio SET isPlaying = 'true' WHERE name = :name")
    suspend fun setPlayForRadio(name: String)

    @Query("UPDATE radio SET isPlaying = 'false' WHERE name = :name")
    suspend fun setPayseForRadio(name: String)
}