package ru.knaus_g.radiored.room

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.knaus_g.radiored.models.Radio
import ru.knaus_g.radiored.models.Settings

@Database(entities = [Radio::class, Settings::class], version = 1, exportSchema = false)
abstract class AppDatabase:RoomDatabase(){
    abstract fun radioDao(): RadioDao
    abstract fun settingsDao(): SettingsDao
}