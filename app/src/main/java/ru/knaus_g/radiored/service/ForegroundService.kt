package ru.knaus_g.radiored.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import dagger.android.DaggerService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import ru.knaus_g.radiored.R
import ru.knaus_g.radiored.room.RadioDao
import ru.knaus_g.radiored.ui.main.MainActivity
import javax.inject.Inject


class ForegroundService : DaggerService() {

    @Inject
    lateinit var radioDao: RadioDao

    private val CHANNEL_ID = "ForegroundService Kotlin"

    override fun onDestroy() {
        super.onDestroy()
        CoroutineScope(IO).launch {
            radioDao.startApp()
        }
    }

    companion object {


        var mMediaPlayer: MediaPlayer? = null


        fun startService(context: Context, message: String, title: String, stream: String) {
            val startIntent = Intent(context, ForegroundService::class.java)
            startIntent.putExtra("message", message)
            startIntent.putExtra("title", title)
            startIntent.putExtra("stream", stream)
            ContextCompat.startForegroundService(context, startIntent)

            if (mMediaPlayer == null) {
                mMediaPlayer = MediaPlayer()
                mMediaPlayer?.setDataSource(stream)
                mMediaPlayer?.prepareAsync()
                mMediaPlayer?.setVolume(50F, 50F)
                mMediaPlayer?.setOnPreparedListener {
                    it.start()
                }

            } else {
                mMediaPlayer?.start()
            }

        }

        fun stopService(context: Context) {
            val stopIntent = Intent(context, ForegroundService::class.java)
            context.stopService(stopIntent)

            mMediaPlayer?.release()
            mMediaPlayer = null

        }

        fun pauseService() {
            if (mMediaPlayer?.isPlaying!!) {
                mMediaPlayer?.pause()
            }
        }
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val message = intent?.getStringExtra("message")
        val title = intent?.getStringExtra("title")
        val stream = intent?.getStringExtra("stream")
        createNotificationChannel()
        val notificationIntent = Intent(this, MainActivity::class.java)

        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT
        )


        // Создание Действий

        // Button CLOSE
        val notificationIntentClose = Intent(this, ForegroundService::class.java)
        notificationIntentClose.putExtra("action", "close")

        val closePendingIntent = PendingIntent.getService(
            this,
            0, notificationIntentClose, PendingIntent.FLAG_CANCEL_CURRENT
        )

        val actionClose: NotificationCompat.Action = NotificationCompat.Action.Builder(
            R.drawable.ic_play_arrow_black_24dp,
            "CLOSE",
            closePendingIntent
        ).build()


        // Button PLAY
        var notificationIntentPlay = Intent(this, ForegroundService::class.java)
        notificationIntentPlay.putExtra("action", "play")
        notificationIntentPlay.putExtra("message", message)
        notificationIntentPlay.putExtra("title", title)
        notificationIntentPlay.putExtra("stream", stream)

        val playPendingIntent = PendingIntent.getService(
            this,
            1, notificationIntentPlay, 0
        )

        val actionPlay: NotificationCompat.Action = NotificationCompat.Action.Builder(
            R.drawable.ic_play_arrow_black_24dp,
            "PLAY",
            playPendingIntent
        ).build()


        // Button PAUSE
        var notificationIntentPause = Intent(this, ForegroundService::class.java)
        notificationIntentPause.putExtra("action", "pause")
        notificationIntentPause.putExtra("message", message)
        notificationIntentPause.putExtra("title", title)

        val pausePendingIntent = PendingIntent.getService(
            this,
            2, notificationIntentPause,
            PendingIntent.FLAG_CANCEL_CURRENT
        )

        val actionPause: NotificationCompat.Action = NotificationCompat.Action.Builder(
            R.drawable.ic_pause_black_24dp,
            "PAUSE",
            pausePendingIntent
        ).build()


        // Button RELOAD
        var notificationIntentReload = Intent(this, ForegroundService::class.java)
        notificationIntentReload.putExtra("action", "reload")
        notificationIntentReload.putExtra("message", message)
        notificationIntentReload.putExtra("title", title)
        notificationIntentReload.putExtra("stream", stream)

        val reloadPendingIntent = PendingIntent.getService(
            this,
            3, notificationIntentReload,
            PendingIntent.FLAG_CANCEL_CURRENT
        )

        val actionReload: NotificationCompat.Action = NotificationCompat.Action.Builder(
            R.drawable.ic_refresh_black_24dp,
            "RELOAD",
            reloadPendingIntent
        ).build()


        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(title)
            .setContentText(message)
            .setSmallIcon(R.drawable.small_icon)
            .setContentIntent(pendingIntent)
            .addAction(actionClose)
            .addAction(actionPause)
            .addAction(actionReload)
            .build()
        startForeground(1, notification)


        // Обработка

        when (intent?.getStringExtra("action")) {
            "pause" -> {

                val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(intent?.getStringExtra("message"))
                    .setSmallIcon(R.drawable.small_icon)
                    .setContentIntent(pendingIntent)
                    .addAction(actionClose)
                    .addAction(actionPlay)
                    .addAction(actionReload)
                    .build()
                startForeground(1, notification)

                val manager = getSystemService(NotificationManager::class.java).notify(1, notification)

                CoroutineScope(IO).launch {
                    if (title != null) {
                        radioDao.setPayseForRadio(title)
                    }
                }

                pauseService()
            }
            "reload" ->{
                Companion.stopService(this)
                Companion.startService(this, intent?.getStringExtra("message"), intent?.getStringExtra("title"), intent?.getStringExtra("stream"))
            }
            "play" -> {
                Companion.startService(this, intent?.getStringExtra("message"), intent?.getStringExtra("title"), intent?.getStringExtra("stream"))
                val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.small_icon)
                    .setContentIntent(pendingIntent)
                    .addAction(actionClose)
                    .addAction(actionPause)
                    .addAction(actionReload)
                    .build()
                val manager = getSystemService(NotificationManager::class.java).notify(1, notification)
                CoroutineScope(IO).launch {
                    if (title != null) {
                        radioDao.setPlayForRadio(title)
                    }
                }
            }
            "close" -> {
                Companion.stopService(this)
            }
        }




        return START_NOT_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID, "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }


}