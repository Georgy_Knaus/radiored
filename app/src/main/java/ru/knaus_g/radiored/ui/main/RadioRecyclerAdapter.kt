package ru.knaus_g.radiored.ui.main

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.layout_radio_item.view.*
import ru.knaus_g.radiored.R
import ru.knaus_g.radiored.models.Radio

class RadioRecyclerAdapter (private val interaction: Interaction? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var radioArray: ArrayList<Radio> = ArrayList()
    var allRadioArray: ArrayList<Radio> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_radio_item,
                parent,
                false
            ),
            interaction
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                holder.bind(radioArray[position])
            }
        }
    }

    override fun getItemCount(): Int {
        return radioArray.size
    }



    fun isPlaying(position: Int): Int{
        return radioArray[position].isPlaying
    }


    fun somebodyTarget(item: Radio): Boolean {

        var bol: Boolean = false

        for (i in 0 until radioArray.size) {
            if (radioArray[i].isPlayerTarget == 0) {
                bol = false
            } else {
                if (radioArray[i] != item) {
                    bol = true
                    return bol
                }
            }
        }
        return bol
    }

    fun submitList(newList: List<Radio>) {
        val oldList = radioArray
        val diffResult: DiffUtil.DiffResult = DiffUtil.calculateDiff(
            RadioDiffCallback(
                oldList,
                newList
            )
        )
        diffResult.dispatchUpdatesTo(this)
        radioArray = newList as ArrayList<Radio>


        allRadioArray.clear()
        allRadioArray.addAll(newList)
    }


    class RadioDiffCallback(
        var oldRadioList: List<Radio>,
        var newRadioList: List<Radio>
    ): DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return (oldRadioList.get(oldItemPosition).name == newRadioList.get(newItemPosition).name)
        }

        override fun getOldListSize(): Int {
            return oldRadioList.size
        }

        override fun getNewListSize(): Int {
            return newRadioList.size
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldRadioList[oldItemPosition].name == newRadioList[newItemPosition].name &&
                    oldRadioList[oldItemPosition].image == newRadioList[newItemPosition].image &&
                    oldRadioList[oldItemPosition].genre == newRadioList[newItemPosition].genre &&
                    oldRadioList[oldItemPosition].radioStream == newRadioList[newItemPosition].radioStream &&
                    oldRadioList[oldItemPosition].isFavorite == newRadioList[newItemPosition].isFavorite &&
                    oldRadioList[oldItemPosition].isPlayerTarget == newRadioList[newItemPosition].isPlayerTarget &&
                    oldRadioList[oldItemPosition].isPlaying == newRadioList[newItemPosition].isPlaying
        }

    }

    class ViewHolder
    constructor(
        itemView: View,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: Radio) = with(itemView) {

            play_pause.setOnClickListener {
                interaction?.onPlayClick(adapterPosition, item)
            }

            setOnLongClickListener {
                interaction?.onLongClick(adapterPosition, item)
                true
            }

            favorite.setOnClickListener {
                interaction?.onFavoriteClick(item)
            }

            radioName.text = item.name

            val requestOptions = RequestOptions()
                .placeholder(R.drawable.white_background)
                .error(R.drawable.white_background)

            Glide.with(itemView)
                .applyDefaultRequestOptions(requestOptions)
                .load(item.image)
                .into(itemView.radioLogo)

            radioGenre.text = item.genre

            if (item.isPlaying == 1) {
                play_pause.setImageResource(R.drawable.ic_pause_black_24dp)
            } else {
                play_pause.setImageResource(R.drawable.ic_play_arrow_black_24dp)
            }

            if(item.isFavorite == 1){
                favorite.setImageResource(R.drawable.ic_star_red_40dp)
            } else{
                favorite.setImageResource(R.drawable.ic_star_border_red_40dp)
            }

        }
    }

    interface Interaction {
        fun onPlayClick(position: Int, item: Radio)

        fun onLongClick(position: Int, item: Radio)

        fun onFavoriteClick(item: Radio)
    }




}
