package ru.knaus_g.radiored.ui.about

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.about_activity.*
import ru.knaus_g.radiored.R

class AboutActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about_activity)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


        email.setOnClickListener {
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_EMAIL, arrayOf<String>(email.text as String))
            intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.about_sent_title))
            val chooseIntent =
                Intent.createChooser(intent, getString(R.string.choose_app))

            startActivity(chooseIntent)
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            android.R.id.home -> onBackPressed()
        }

        return true
    }
}