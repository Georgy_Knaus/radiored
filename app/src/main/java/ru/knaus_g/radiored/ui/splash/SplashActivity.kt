package ru.knaus_g.radiored.ui.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.knaus_g.radiored.ui.main.MainActivity

class SplashActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val intent = Intent(this@SplashActivity, MainActivity::class.java)

        Thread.sleep(2000)

        startActivity(intent)
        finish()
    }
}