package ru.knaus_g.radiored.ui.main

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.MenuItem.OnActionExpandListener
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import ru.knaus_g.radiored.R
import ru.knaus_g.radiored.TopSpacingItemDecoration
import ru.knaus_g.radiored.models.Radio
import ru.knaus_g.radiored.models.Settings
import ru.knaus_g.radiored.service.ForegroundService
import ru.knaus_g.radiored.ui.about.AboutActivity
import javax.inject.Inject

@Suppress("PLUGIN_WARNING")
class MainActivity : DaggerAppCompatActivity(),
    RadioRecyclerAdapter.Interaction {

    var favorite: Int? = 0


    lateinit var radioAdapter: RadioRecyclerAdapter

    @Inject
    lateinit var factory: ViewModelProvider.Factory

    val model: MainViewModel by lazy {
        ViewModelProviders.of(this, factory)[MainViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()

        model.getSettings("MainFavorite").observe(this, Observer {
            favorite = it.value

            if (model.getSearchData().value!!.length > 0) {
                model.changeFavorite(null)
            } else {

                when (favorite) {
                    0 -> {
                        model.changeFavorite(true)
                    }
                    1 -> {
                        model.changeFavorite(false)
                    }
                }
            }
        })


        model.items.observe(this, Observer {
            radioAdapter.submitList(it)
            if(it.isEmpty()){
                this.search_is_empty.text = getString(R.string.not_found)
            }else{
                this.search_is_empty.text = (getString(R.string.text_null))
            }
        })


        if (savedInstanceState == null) {
            CoroutineScope(IO).launch {
                model.deleteTarget()
            }
        }
    }


    private fun Activity.initRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            val topSpacingItemDecoration =
                TopSpacingItemDecoration(30)
            addItemDecoration(topSpacingItemDecoration)
            radioAdapter =
                RadioRecyclerAdapter(this@MainActivity)
            adapter = radioAdapter
        }
    }


    override fun onPlayClick(position: Int, item: Radio) {

        if (radioAdapter.isPlaying(position) == 1) {

            model.stopService()

            CoroutineScope(IO).launch {
                val radio: Radio = Radio(
                    item.name,
                    item.radioStream,
                    item.image,
                    item.genre,
                    0,
                    0,
                    item.isFavorite
                )
                model.updateRadio(radio)
            }
        } else {
            if (radioAdapter.somebodyTarget(item)) {
                ForegroundService.stopService(this)
                CoroutineScope(IO).launch {
                    model.deleteTarget()
                }
            }

            CoroutineScope(IO).launch {
                model.stopAll(item.name)
                val radio: Radio = Radio(
                    item.name,
                    item.radioStream,
                    item.image,
                    item.genre,
                    1,
                    1,
                    item.isFavorite
                )
                model.updateRadio(radio)
            }
            model.startService(item)
        }
    }


    override fun onLongClick(position: Int, item: Radio) {

        val callback: AreYouSureCallback = object :
            AreYouSureCallback {
            override fun proceed() {
                Toast.makeText(
                    this@MainActivity,
                    "Radio ${item.name} has been deleted",
                    Toast.LENGTH_SHORT
                ).show()
                CoroutineScope(IO).launch {
                    model.deleteRadio(item)
                }
            }

            override fun cancel() {
            }
        }
        displayDeleteDialog("Are you sure?", callback)
    }

    override fun onFavoriteClick(item: Radio) {

        if (item.isFavorite == 1) {
            val radio: Radio = Radio(
                item.name,
                item.radioStream,
                item.image,
                item.genre,
                item.isPlaying,
                item.isPlayerTarget,
                0
            )
            CoroutineScope(IO).launch {
                model.updateRadio(radio)
            }
        } else {
            val radio: Radio = Radio(
                item.name,
                item.radioStream,
                item.image,
                item.genre,
                item.isPlaying,
                item.isPlayerTarget,
                1
            )
            CoroutineScope(IO).launch {
                model.updateRadio(radio)
            }
        }
    }


    private fun displayDeleteDialog(message: String?, callback: AreYouSureCallback) {
        MaterialDialog(this).show {
            title(R.string.deleting)
            message(text = message)
            positiveButton(R.string.text_ok) {
                callback.proceed()
            }
            negativeButton(R.string.cancel) {
                callback.cancel()
            }

        }
    }

    interface AreYouSureCallback {
        fun proceed()
        fun cancel()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)

        val menuItem = menu?.findItem(R.id.action_find)
        val actionSort = menu?.findItem(R.id.action_favorite)
        val actionAbout = menu?.findItem(R.id.action_about)

        if (favorite == 1) {
            actionSort?.setIcon(R.drawable.ic_star_black_24dp)
        } else {
            actionSort?.setIcon(R.drawable.ic_star_border_black_24dp)
        }




        menuItem?.setOnActionExpandListener(object : OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                actionSort?.isVisible = false
                actionAbout?.isVisible = false
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                actionSort?.isVisible = true
                actionAbout?.isVisible = true
                return true
            }
        })

        if (menuItem != null) {

            val searchView = menuItem.actionView as SearchView
            if (model.getSearchData().value!!.isNotEmpty()) {
                menuItem.expandActionView()
                searchView.onActionViewExpanded()
                searchView.setQuery(model.getSearchData().value, true)

            }



            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    model.changeFavorite(null)
                    model.setQuery(newText)
                    return true
                }
            })
        }
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.action_favorite -> {
                if (favorite == 1) {
                    item.setIcon(R.drawable.ic_star_border_black_24dp)

                    model.changeFavorite(true)

                    CoroutineScope(IO).launch {
                        model.updateSettings(Settings("MainFavorite", 0))
                    }

                } else {
                    item.setIcon(R.drawable.ic_star_black_24dp)

                    model.changeFavorite(false)

                    CoroutineScope(IO).launch {
                        model.updateSettings(Settings("MainFavorite", 1))
                    }
                }
            }

            R.id.action_about -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}


