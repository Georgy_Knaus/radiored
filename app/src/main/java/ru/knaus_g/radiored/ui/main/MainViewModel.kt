package ru.knaus_g.radiored.ui.main

import android.content.Context
import androidx.arch.core.util.Function
import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import ru.knaus_g.radiored.models.Radio
import ru.knaus_g.radiored.models.Settings
import ru.knaus_g.radiored.room.RadioDao
import ru.knaus_g.radiored.room.SettingsDao
import ru.knaus_g.radiored.service.ForegroundService
import java.lang.StringBuilder
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private var radioDao: RadioDao, private var settingsDao: SettingsDao,
    private val context: Context
) : ViewModel() {

    val searchLiveData = MutableLiveData<String>("")
    val sortLiveData = MutableLiveData(false)


    val items = searchLiveData.switchMap { query ->
        sortLiveData.map { Pair(query, it) }
    }.switchMap {

        when(it.second){
            true -> getAllRadioSortName()
            false -> getAllRadioFavorite()
            null -> searchFromData(it.first)
        }
    }

    fun setQuery(query:String){
        searchLiveData.value = query
    }

    fun getSearchData(): LiveData<String> {
        return searchLiveData
    }

    fun changeFavorite(isAlphabet: Boolean?) {
        sortLiveData.value = isAlphabet
    }

    fun searchFromData(name: String): LiveData<List<Radio>> {

        if(name.length>0){
            val query: StringBuilder = StringBuilder()
            query.append("%").append(name).append("%")
            return radioDao.searchFromData(query.toString())
        } else{
            return getAllRadioSortName()
        }
    }


    fun getAllRadioSortName(): LiveData<List<Radio>> {
        return radioDao.getAllRadioSortName()
    }


    fun getAllRadioFavorite(): LiveData<List<Radio>> {
        return radioDao.getAllRadioFavorite()
    }


    suspend fun updateRadio(radio: Radio) {
        radioDao.updateRadio(radio)
    }

    suspend fun deleteTarget() {
        radioDao.deleteTarget()
    }

    suspend fun stopAll(radioName: String) {
        radioDao.stopAll(radioName)
    }


    suspend fun deleteRadio(radio: Radio) {
        radioDao.deleteRadio(radio)
    }

    fun startService(item: Radio) {
        ForegroundService.startService(
            context,
            "Radio ${item.name} is playing",
            item.name,
            item.radioStream
        )
    }


    fun stopService() {
        ForegroundService.stopService(context)
    }


    fun getSettings(name: String): LiveData<Settings> {
        return settingsDao.getSettings(name)
    }


    suspend fun updateSettings(setting: Settings) {
        return settingsDao.updateSettings(setting)
    }

}